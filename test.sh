#!/bin/sh -ex
pip install -e .[test]
isort --check .
black --check .
flake8
pylint pypi_version_check spec/*.py *.py
bandit -r .
mamba --enable-coverage
coverage report --fail-under=100
pypi-version-check
