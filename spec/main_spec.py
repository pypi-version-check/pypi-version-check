#
#   Copyright © 2021 Simó Albert i Beltran
#
#   This file is part of PyPI Version Check.
#
#   Mkdocs i18n plugin is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   Foobar is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with MkDocs i18n plugin. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

""" Tests for PyPI Version Check """

from unittest.mock import Mock, call, patch

from click.testing import CliRunner
from expects import equal, expect
from mamba import description, it

from pypi_version_check.__main__ import cli, init, main


def json():
    """Mock json response of PyPI

    :rtype: dict
    :return: Dictionary with releases as keys
    """
    return {"releases": {"0.0.0": [], "0.0.1": [], "0.1.0": [], "1.0.0": []}}


with description("PyPI Version Check") as self:
    with it("runs cli with PyPI URLs as arguments and fail"):
        with patch("pypi_version_check.__main__.main") as mock_main:
            mock_main.return_value = True
            runner = CliRunner()
            result = runner.invoke(cli, ["https://pypi.org", "https://test.pypi.org"])
            mock_main.assert_called_once_with(
                ("https://pypi.org", "https://test.pypi.org")
            )
            expect(result.exit_code).to(equal(1))
    with it("runs cli with PyPI URLs as arguments and not fail"):
        with patch("pypi_version_check.__main__.main") as mock_main:
            mock_main.return_value = False
            runner = CliRunner()
            result = runner.invoke(cli, ["https://pypi.org", "https://test.pypi.org"])
            mock_main.assert_called_once_with(
                ("https://pypi.org", "https://test.pypi.org")
            )
            expect(result.exit_code).to(equal(0))
    with it("run as module with PyPI URLs as arguments"):
        with patch("pypi_version_check.__main__.__name__", "__main__"), patch(
            "pypi_version_check.__main__.cli"
        ) as mock_cli:
            init()
            mock_cli.assert_called_once()
    with it("fails on distribution not found"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "pypi_version_check.__main__.logger.error"
        ) as mock_error:
            mock_distributions.return_value = ()
            expect(main()).to(equal(True))
            mock_error.assert_called_once_with("Distribution not found")
    with it("fails on unknow HTTP code"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "requests.get"
        ) as mock_request, patch(
            "pypi_version_check.__main__.logger.error"
        ) as mock_error:
            distribution = Mock(project_name="projectname")
            mock_distributions.return_value = iter([distribution])
            mock_request.return_value.status_code = 401
            expect(main()).to(equal(True))
            mock_error.assert_called_once_with("PyPI returns %s as status_code", 401)
    with it("not fails on package not found"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "requests.get"
        ) as mock_request, patch(
            "pypi_version_check.__main__.logger.warning"
        ) as mock_warning:
            distribution = Mock(project_name="projectname")
            mock_distributions.return_value = iter([distribution])
            mock_request.return_value.status_code = 404
            expect(main()).to(equal(False))
            mock_warning.assert_called_once_with(
                "Package %s not found in PyPI", "projectname"
            )
    with it("not fails on version not found"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "requests.get"
        ) as mock_request, patch(
            "pypi_version_check.__main__.logger.info"
        ) as mock_info:
            distribution = Mock(project_name="projectname", version="0.1.1")
            mock_distributions.return_value = iter([distribution])
            mock_request.return_value.status_code = 200
            mock_request.return_value.json = json
            expect(main()).to(equal(False))
            mock_request.assert_called_once_with(
                "https://pypi.org/pypi/projectname/json"
            )
            mock_info.assert_called_once_with(
                "PyPI hasn't the version %s for %s", "0.1.1", "projectname"
            )
    with it("fails on version found"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "requests.get"
        ) as mock_request, patch(
            "pypi_version_check.__main__.logger.error"
        ) as mock_error:
            distribution = Mock(project_name="projectname", version="0.1.0")
            mock_distributions.return_value = iter([distribution])
            mock_request.return_value.status_code = 200
            mock_request.return_value.json = json
            expect(main()).to(equal(True))
            mock_request.assert_called_once_with(
                "https://pypi.org/pypi/projectname/json"
            )
            mock_error.assert_called_once_with(
                "PyPI has the version %s for %s", "0.1.0", "projectname"
            )
    with it("fails if one distribution was published"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "requests.get"
        ) as mock_request, patch(
            "pypi_version_check.__main__.logger.error"
        ) as mock_error:
            distribution1 = Mock(project_name="projectname", version="0.1.1")
            distribution2 = Mock(project_name="projectname", version="0.1.0")
            mock_distributions.return_value = iter([distribution1, distribution2])
            mock_request.return_value.status_code = 200
            mock_request.return_value.json = json
            expect(main()).to(equal(True))
            mock_request.assert_has_calls(
                [call("https://pypi.org/pypi/projectname/json")] * 2
            )
            mock_error.assert_called_once_with(
                "PyPI has the version %s for %s", "0.1.0", "projectname"
            )
    with it("requests to custom PyPI URLs"):
        with patch("pkg_resources.find_distributions") as mock_distributions, patch(
            "requests.get"
        ) as mock_request, patch(
            "pypi_version_check.__main__.logger.info"
        ) as mock_info:
            distribution = Mock(project_name="projectname", version="0.1.1")
            mock_distributions.return_value = iter([distribution])
            mock_request.return_value.status_code = 200
            mock_request.return_value.json = json
            pypi_urls = ["https://pypi.org", "https://test.pypi.org"]
            expect(main(pypi_urls)).to(equal(False))
            mock_request.assert_has_calls(
                [
                    call("https://pypi.org/pypi/projectname/json"),
                    call("https://test.pypi.org/pypi/projectname/json"),
                ]
            )
            mock_info.assert_has_calls(
                [call("PyPI hasn't the version %s for %s", "0.1.1", "projectname")] * 2
            )
