# PyPI Version Check

Tool to check if PyPI publishes the current package version. Useful as a test in CI to force version change.

See also how is used by this project via [.gitlab-ci](https://gitlab.com/pypi-version-check/pypi-version-check/-/blob/main/.gitlab-ci.yml#L6) that runs it via [script](https://gitlab.com/pypi-version-check/pypi-version-check/-/blob/main/test.sh#L10)

## Donations:

- [Liberapay](https://liberapay.com/sim6/donate)
- [Bitcoin](bitcoin:15QqofyoWxDSZU9VbXwVZKFxAVdmpkE5uH?message=pypi-version-check)

## Other works:

- [pypi-version](https://pypi.org/project/pypi-version/): Abandoned and requires TravisCI or CicleCI
